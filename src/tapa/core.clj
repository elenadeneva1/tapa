(ns tapa.core
  (:gen-class)
  (:use tapa.ui.init
        tapa.gui.init))

(defn -main
  "Start playing game"
  [& args]
  (if (and (< 0 (count args)) (= (first args) "gui"))
    (start-gui-game)
    (start-ui-game)))
