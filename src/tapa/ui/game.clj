(ns tapa.ui.game
  (:gen-class)
  (:use tapa.base.move
        tapa.base.init
        tapa.base.utils
        tapa.base.ai
        [clojure.string :only [join trim]]))

(defn get-char [pools index]
  (cond
     (>= index (count pools)) "  "
     (= 0 (nth pools index)) "\u25CF "
     :else "\u25CB "))

(defn print-row [board row asc]
  (if asc
    (do
      (print "|"(join "  "(map #(get-char (board %) row) (range 1 7))))
      (println " |"(join "  "(map #(get-char (board %) row) (range 7 13)))"|"))
    (do
      (print "|"(join "  "(map #(get-char (board %) row) (range 24 18 -1))))
      (println " |"(join "  "(map #(get-char (board %) row) (range 18 12 -1)))"|"))))

(defn print-game [game]
  (println "\n*"(join "-" (map (constantly "-") (range 1 25)))"*")
  (if (zero? (@game :next))
    (do
      (println "| "(join "   " (range 1 7))"| "(join "   " (range 7 10))""(join "  " (range 10 13))"|")
      (dotimes [n 7] (print-row (@game :board) n true))
      (println "|                        |                        |")
      (dotimes [n 7] (print-row (@game :board) (- 6 n) false))
      (println "|" (join "  " (range 24 18 -1))"|"(join "  " (range 18 12 -1))"|"))
    (do
      (println "|" (join "  " (range 24 18 -1))"|"(join "  " (range 18 12 -1))"|")
      (dotimes [n 7] (print-row (@game :board) n false))
      (println "|                        |                        |")
      (dotimes [n 7] (print-row (@game :board) (- 6 n) true))
      (println "| "(join "   " (range 1 7))"|"(join "   " (range 7 10))""(join "  " (range 10 13))" |")))
  (println "*"(join "-" (map (constantly "-") (range 1 25)))"*")
  (println "Next is player with pool ->"(get-char [(@game :next)] 0)))

(defn process-move [game steps]
  (println "Enter position of pool to move with"steps"steps")
  (loop [pos (read)]
    (when-not (try-move pos steps game)
      (println "Invalid move! Pleade enter valid position to move " steps)
      (recur (read)))))

(defn order [dice]
  (println "Type the move you want first to play")
  (loop [first-dice (read)]
    (cond
      (= first-dice (first dice)) [0 1]
      (= first-dice (second dice)) [1 0]
      :else
       (do
         (println "You have: " (first dice) (second dice) "Enter valid dice! Type the move you want first to play")
         (recur (read))))))

(defn player-turn [game]
  (roll-dice game)
  (when (no-more-moves? game)
    (prn "No possible moves!!! NEXT PLAYER'S turn" (clojure.string/join " " (@game :dices)))
    (change-players game)
    (swap! game assoc :dices [])
    (roll-dice))
  (let [dice (@game :dices)]
    (print-game game)
    (println "Dice -> "(first dice) (second dice))
    (if (pair? dice)
       (dotimes [n 4]
         (do
           (process-move game (first dice))
           (remove-dice game 0)
           (if-not (= n 3) (print-game game))))
       (let [dice-order (order dice)]
         (process-move game (nth (@game :dices) (first dice-order)))
         (remove-dice game (first dice-order))
         (print-game game)
         (process-move game (first (@game :dices)))
         (remove-dice game 0)))))

(defn computer-game [game players-turn]
  (println "You are " (if (zero? players-turn) "first" "second"))
  (loop [i players-turn]
    (if (zero? (rem i 2))
      (player-turn game)
      (do
        (roll-dice game)
        (print "Computer move "  (clojure.string/join " " (@game :dices)))
        (computer-turn game)))
    (when-not (win? game)
      (change-players game)
      (recur (inc i)))))

(defn two-player-game []
  (let [game (new-game)]
    (loop []
      (player-turn game)
      (when-not (win? game)
        (change-players game)
        (recur)))))

(defn read-player-count []
  (let [info "Please enter player count : \n(To play with computer enter 1, otherwise 2)"]
    (println info)
    (loop [player-count (trim (read-line))]
      (if (#{"1" "2"} player-count)
        player-count
        (do (println info) (recur (trim (read-line))))))))

(defn start-new-game []
  (let [player-count (read-player-count)]
    (if (= player-count "2")
      (two-player-game)
      (computer-game (new-game) (rand-int 2)))))
