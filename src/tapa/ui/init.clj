(ns tapa.ui.init
  (:gen-class)
  (:use tapa.ui.game
        tapa.base.utils))

(defn start-ui-game []
  (println (greeting))
  (println "\n\n*******************************************")
  (println "*              Game started               *")
  (println "*******************************************\n")
  (start-new-game))
