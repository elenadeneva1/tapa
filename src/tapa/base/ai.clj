(ns tapa.base.ai
  (:gen-class)
  (:use tapa.base.move
        tapa.base.utils))

(defn all-possible-moves [game steps]
  "Return all possible moves for the next player"
  (let [next-player (get-next-player game)
        steps (* steps (player-direction next-player))]
    (vec (filter #(is-valid-move? % steps next-player (@game :board)) (range 1 25)))))

(defn no-more-moves? [game]
  "Check if there are no more moves"
  (every? #(empty? (all-possible-moves game %)) (@game :dices)))

(defn choose-first-dice
  "Choose the dice the computer will move first"
  [game] 0)

(defn process-computer-move [game steps]
  "Realize one move of the computer (Include all the moves)"
  (let [all-moves (all-possible-moves game steps)
        next-player (get-next-player game)
        other-player (opponent game)]
    (when-not (empty? all-moves)
      (let [pos (nth all-moves (rand-int (count all-moves)))]
        (try-move pos steps game)))))

(defn process-pair-move [game]
  "Move the dices"
  (loop []
    (process-computer-move game (first (@game :dices)))
    (remove-dice game 0)
    (when-not (or (empty? (@game :dices)) (win? game) (no-more-moves? game))
      (recur))))

(defn process-nonpair-move [game]
  "Choose order of the dices and than move it"
  (let [first-move (choose-first-dice game)]
    (process-computer-move game (nth (@game :dices) first-move))
    (remove-dice game first-move)
    (when-not (or (win? game) (no-more-moves? game))
      (process-computer-move game (first (@game :dices))))))

(defn computer-turn [game]
  "Realize one turn of the computer (Include all the moves)!"
  (when-not (no-more-moves? game)
    ((if (= (count (@game :dices)) 4) process-pair-move process-nonpair-move) game))
  (swap! game assoc :dices []))
