(ns tapa.base.move
  (:gen-class)
  (:use tapa.base.init
        tapa.base.utils))

(defn is-valid-move?
  "Check if the player can move from position
   pos piece with moves on the board"
  [pos moves player board]
  (let [to (+ pos moves) pieces (board to)]
    (every? true?
       [(< 0 pos 25)
        (< 0 (abs moves) 7)
        (not (empty? (board pos)))
        (= (last (board pos)) (player :number))
        (or (and (not (<= 1 to 24)) (= 15 (player :home)))
            (and (<= 1 to 24) (or (<= (count pieces) 1) (= (last pieces) (player :number)))))])))

(defn move
  "Move piece on the board"
  [from to board]
  (if (< 0 to 25)
    (assoc board to (conj (board to) (last (board from))) from (vec (drop-last (board from))))
    (assoc board from (vec (drop-last (board from))))))

(defn inc-property [map-atom property]
  (assoc map-atom property (inc (map-atom property))))

(defn update
  "Update the info about the player if the piece
   is now out of the board or in the home"
  [from moves next-player]
  (let [to (+ from moves)]
    (cond
     (not (< 0 to 25)) (inc-property next-player :out)
     (and (< 18 to) (< from 19)) (inc-property next-player :home)
     (and (< to 7) (< 6 from)) (inc-property next-player :home)
     :else next-player)))

(defn try-move
  [from moves game]
  (let [next-player (get-next-player game)
        other-player (opponent game)
        board (@game :board)
        moves (* moves (player-direction next-player))
        valid (is-valid-move? from moves next-player board)]
    (when valid
      (let [updated-player (update from moves next-player)]
        (swap! game assoc
         :board (move from (+ moves from) board)
         :players ((if (zero? (@game :next)) identity reverse)
           [updated-player other-player]))))
    valid))
