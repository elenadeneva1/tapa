(ns tapa.base.utils
  (:gen-class)
  (:use tapa.base.init
        [clojure.string :only [split]]))

(defn abs [x] (if (neg? x) (* -1 x) x))

(defn pair? [dice]
  (= 1 (-> dice set count)))

(defn double-if-pair [dice]
  (vec (if (pair? dice) (repeat 4 (first dice)) dice)))

(defn get-dices
  "Generate two ranodom number representing dices"
  [] (double-if-pair (take 2 (repeatedly #(inc (rand-int 6))))))

(defn remove-index
  "Remove the element of position index from the array"
  [array index]
    (vec (concat (take index array) (drop (inc index) array))))

(defn player-direction [player]
  (if (zero? (player :number)) 1 -1))

(defn remove-dice [game index]
  (swap! game assoc :dices (remove-index (@game :dices) index)))

(defn roll-dice [game]
  (let [result (empty? (@game :dices))]
    (when result
      (swap! game assoc :dices (get-dices)))
    result))

(defn get-next-player [game]
  (nth (@game :players) (@game :next)))

(defn win? [game]
  (= 15 ((get-next-player game) :out)))

(defn change-players [game]
  (swap! game assoc :next (rem (inc (@game :next)) 2)))

(defn opponent [game]
  (nth (@game :players) (rem (inc (@game :next)) 2)))

(defn greeting []
  (clojure.string/replace
    (first
      (split
        (slurp "http://www.iheartquotes.com/api/v1/random")
        #"http://iheartquotes.com"))
    #"&quot;" ""))
