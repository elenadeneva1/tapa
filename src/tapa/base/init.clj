(ns tapa.base.init
  (:gen-class))

(defn new-board []
  "Create a new board where the white's pieces are on position 1
   black's pieces are on position 24 and the other are empty"
  (zipmap
    (range 1 25)
    (concat
      [(vec (map (constantly 0) (range 1 16)))]
      (map (constantly []) (range 2 24))
      [(vec (map (constantly 1) (range 1 16)))])))

(defn new-player [number]
  "Create a new player with
   number --> 0(White) 1(Black)
   home --> pieces in the final six position for this player
   out  --> pieces out of the board for this player"
  {:number number
   :home 0
   :out 0})

(defn new-game []
  (atom {:board (new-board)
         :players (map new-player [0 1])
         :next 0
         :dices []
         :computer -1}))
