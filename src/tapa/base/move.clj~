(ns tapa.base.move
  (:gen-class)
  (:use tapa.base.init)
  (:use tapa.base.utils))

(defn is-valid-move? [pos moves next-player board]
  (if (and (< 0 pos 25) (< 0 (abs moves) 7))
    (if (or (empty? (@board pos)) (not= (last (@board pos)) (@next-player :number)))
      false
      (let [to (+ pos moves) pieces (@board :to)]
        (if (and (or (< to 1) (> to 24)) (not= (@next-player :home) 14))
          false
          (or (<= (count pieces) 1) (= (last pieces) (@next-player :number))))))
     false))

(defn move [from to board]
  (when (< 0 to 25)
    (swap! board assoc to (conj (@board to) (last (@board from)))))
  (swap! board assoc from (drop-last (@board from))))

(defn update [from moves next-player other-player board]
  (when (= (count (@board from)) 1)
    (swap! other-player assoc :possible-moves (conj (@other-player :possible-moves) from)))
  (let [to (+ from moves)]
   (if-not (< 0 to 25)
     (swap! next-player assoc :out (inc (@next-player :out)))
     (do
       (when (or (and (< 18 to) (< from 19))  (and (< to 7) (< 6 from)))
         (swap! next-player assoc :home (conj (@next-player :home) (abs to))))
       (when (= (count (@board to)) 2)
         (swap! next-player assoc :possible-moves (conj (@next-player :possible-moves) to))
         (swap! other-player assoc :possible-moves (disj (@other-player :possible-moves) to)))))))

(defn try-move [from moves next-player other-player board]
  (let [valid (is-valid-move? from moves next-player board)]
    (move from (+ moves from) board)
    (update from moves next-player other-player board)
    valid))
