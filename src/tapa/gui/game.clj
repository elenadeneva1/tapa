(ns tapa.gui.game
  (:gen-class)
  (:use tapa.base.move
        tapa.base.init
        tapa.base.utils
        tapa.base.ai

        seesaw.core
        seesaw.font))

(def f (frame :title "Tapa"))

(defn display [content] (config! f :content content))

(defn default-colors [widget] (if (= "" widget) "" (config! widget :background "#E5BE64" :foreground "#5A2909")))

(defn pressed [widget] (config! widget :background "#deac38" :foreground "#5A2909"))

(defn refresh [game] (prn "Not implemented yet"))

(defn roll [game]
  (when-not (roll-dice game)
    (swap! game assoc :infolabel "First move!"))
  (when (no-more-moves? game)
    (change-players game)
    (swap! game assoc :infolabel  (clojure.string/join " " (@game :dices)) :dices []))
  (refresh game))

(defn process [row col game]
  (if (or (zero? (@game :selected-dice))
          (not (try-move col (@game :selected-dice) game)))
    (do
      (swap! game assoc :infolabel "Invalid!")
      (refresh game))
    (if (win? game)
      (display (default-colors (label :text "You win! :)" :halign :center :h-text-position :center)))
      (do
        (swap! game assoc :infolabel "")
        (remove-dice game (@game :selected-dice-index))
        (when (empty? (@game :dices)) (change-players game))
        (swap! game assoc :selected-dice-index -1 :selected-dice 0)
        (when (and (= (@game :computer) (@game :next)) (empty? (@game :dices)))
          (do
            (roll game)
            (swap! game assoc :infolabel (clojure.string/join " " (@game :dices)))
            (refresh game)
            (computer-turn game)
            (change-players game)))
        (if (win? game)
          (display (default-colors (label :text "Computer wins! :(" :halign :center :h-text-position :center)))
          (refresh game))))))

(defn get-icon [player] (if (zero? player) "white.jpg" "black.jpg"))

(defn make-button [row col game]
  (if (>= row (count (get (@game :board) col)))
    ""
    (let [new-button (button :icon (clojure.java.io/resource (get-icon (nth (get (@game :board) col) row))))]
      (listen new-button :action (fn [e] (process row col game)) :mouse-entered pressed :mouse-exited default-colors)
      new-button)))

(defn first-player-pools [game]
  (let [rows 7 cols 12]
    (for [row (range rows) col (range 1 (inc cols))]
      (make-button row col game))))

(defn second-player-pools [game]
  (let [rows 7 cols 12]
    (for [row (range rows) col (range cols)]
      (make-button (- 6 row) (- 24 col) game))))

(defn select-dice [game index num]
  (swap! game assoc :selected-dice-index index :selected-dice num))

(defn dice-buttons [game index]
  (let [dices (@game :dices) num (nth dices index)
        new-button (button :icon (clojure.java.io/resource (str num ".jpg")))]
    (default-colors new-button)
    (listen new-button :action
      (fn [e]
        (config! e :background "#deffff")
        (select-dice game index num)) :mouse-entered pressed :mouse-exited default-colors)
    new-button))

(defn dices [game]
  (let [buttons (map #(dice-buttons game %) (range 0 (count (@game :dices))))
        buttons-count (count buttons)]
    (if (< buttons-count 4)
      (concat buttons (map (constantly "") (range 0 (- 4  buttons-count))))
      buttons)))

(defn board-buttons [game]
  (map default-colors (flatten
    (vector
      (map #(label :text % :halign :center :h-text-position :center) (range 1 13))
      (first-player-pools game)
      [(@game :roll) (label (if (zero? (@game :next)) "White" "Black"))]
      (dices game)
      [(label (@game :infolabel))]
      (vec (map (constantly "") (range 0 5)))
      (second-player-pools game)
      (map #(label :text % :halign :center :h-text-position :center) (range 24 12 -1))))))

(defn roll-button [game]
  (let [new-button (button :icon  (clojure.java.io/resource "dice.jpg"))]
    (default-colors new-button)
    (listen new-button :action (fn [_] (roll game)) :mouse-entered pressed :mouse-exited default-colors)
    new-button))

(defn make-board-panel [game]
  (swap! game assoc :roll (roll-button game))
  (grid-panel :columns 11 :rows 17 :items (board-buttons game)))

(defn refresh [game]
   (let [board-panel (make-board-panel game)]
    (default-colors board-panel)
    (display board-panel)))
