(ns tapa.gui.init
  (:gen-class)
  (:use seesaw.core
        seesaw.font
        seesaw.mig

        tapa.gui.game
        tapa.base.utils
        tapa.base.init
        tapa.base.ai))

(defn start-game-with-two-players [game]
  (swap! game assoc :infolabel  "Play! :)" :selected-dice 0 :selected-dice-index -1)
  (let [board-panel (make-board-panel game)]
    (default-colors board-panel)
    (config! f :size [600 :by 1200])
    (display board-panel)))

(defn start-game-with-computer [game]
  (swap! game assoc :infolabel  "Play! :)" :computer (rand-int 2))
  (let [board-panel (make-board-panel game)]
    (if (zero? (@game :computer))
      (do
        (roll game)
        (swap! game assoc :infolabel (clojure.string/join " " (@game :dices)))
        (computer-turn game)
        (change-players game))
      (swap! game assoc :infolabel "Play!"))
    (default-colors board-panel)
    (config! f :size [600 :by 1200])
    (display board-panel))
  (refresh game))

(defn start-gui-game []
  (native!)
  (let [dim [200 :by 50]
        choose-player (label :text "Choose players count:" :size dim)
        one-player (button :icon (clojure.java.io/resource "one.jpg") :size dim :listen [:action (fn[_] (start-game-with-computer (new-game))) :mouse-entered pressed :mouse-exited default-colors])
        two-players (button :icon (clojure.java.io/resource "two.jpg") :size dim :listen [:action (fn[_] (start-game-with-two-players (new-game))) :mouse-entered pressed :mouse-exited default-colors])]
    (display (default-colors (vertical-panel :items (map default-colors [choose-player one-player two-players]))))
    (-> f pack! show!)))
