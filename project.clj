(defproject tapa "0.1.0-SNAPSHOT"
  :description "Tapa Board Game"
  :url "https://bitbucket.org/elenadeneva1/tapa"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [seesaw "1.4.2"]]
  :main tapa.core
  :aot :all
  :repl-options {:init (use 'tapa.base.ai 'tapa.base.init 'tapa.base.move 'tapa.base.utils
                            'tapa.gui.game 'tapa.gui.init 'tapa.ui.game 'tapa.ui.init)}
  :target-path "target/%s")
