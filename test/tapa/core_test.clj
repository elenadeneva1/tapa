(ns tapa.core-test
  (:use clojure.test
  		tapa.base.init
      tapa.base.utils
      tapa.base.move
      tapa.ui.game
      tapa.base.ai))

(def board (new-board))
(def player0 (new-player 0))
(def player1 (new-player 1))
(defn test-player
  ([home out] {:number 0 :home home :out out})
  ([home out i] {:number i :home home :out out}))

(defn modify-board [pos pools game]
  (swap! game assoc :board (assoc (@game :board) pos pools)))

(deftest board-test
  (testing "Initialize board"
    (is (= (count board) 24))
    (is (= (count (board 1)) 15))
    (is (= (count (board 24)) 15))
    (is (every? #{0} (board 1)))
    (is  (every? #{1} (board 24)))
    (is (every? #(empty? (board %)) (range 2 24)))))

(deftest player-test
  (testing "Initialize players"
    (is (= (:home player0) 0))
    (is (= (:home player1) 0))
    (is (= (:number player0) 0))
    (is (= (:number player1) 1))
    (is (= (:out player0) 0))
    (is (= (:out player1) 0))))

(deftest roll-dice-test
  (testing "Roll dice"
    (let [game (new-game)]
      (is (empty? (@game :dices)))
      (is (true? (roll-dice game)))
      (is (complement (empty? (@game :dices))))
      (let [dices (@game :dices)]
        (is (false? (roll-dice game)))
        (is (= dices (@game :dices)))))))

(deftest remove-dice-test
  (testing "Remove dice"
    (let [game (new-game)]
       (roll-dice game)
       (let [game-dices (@game :dices) cnt (count game-dices)]
         (remove-dice game 1)
         (is (= (dec cnt) (count (@game :dices))))
         (is (false? (roll-dice game)))
         (remove-dice game 0)
         (is (= (- cnt 2) (count (@game :dices))))
         (is (= (vec (rest (rest game-dices))) (@game :dices)))
         (when-not (empty? (@game :dices))
           (remove-dice game 0)
           (remove-dice game 0))
         (is (true? (roll-dice game)))
         (is (false? (empty? (@game :dices))))))))

(deftest remove-index-test
  (testing "Remove element in array by index"
    (is (= [0 1 2 4] (remove-index [0 1 2 3 4] 3)))
    (is (= [] (remove-index [1] 0)))
    (is (= [1] (remove-index [1] -1)))
    (is (= [1] (remove-index [1] 1)))
    (is (= [] (remove-index [2] 0)))
    (is (= [1 2 3 4] (remove-index [0 1 2 3 4] 0)))
    (is (= [0 1 2 3] (remove-index [0 1 2 3 4] 4)))
    (is (= [0 1 3 4] (remove-index [0 1 2 3 4] 2)))
    (is (= [0 1 2 3 4] (remove-index [0 1 2 3 4] -1)))
    (is (= [0 1 2 3 4] (remove-index [0 1 2 3 4] -12)))
    (is (= [0 1 2 3 4] (remove-index [0 1 2 3 4] 5)))
    (is (= [0 1 2 3 4] (remove-index [0 1 2 3 4] 7)))))

(deftest get-dices-test
  (testing "Generate two random numbers representing the dices"
    (is (every? #(and (< 0 (first %) 7) (< 0 (second %) 7) (= (if (= 4 (count %)) 1 2) (-> % set count))) (take 100 (repeatedly #(get-dices)))))))

(deftest double-if-pair-test
  (testing "double-if-pair function test (convert pair of dice to moves ([2 2] -> [2 2 2 2]) [1 2] -> [1 2]"
    (map #(is (= [% % % %] (double-if-pair [% %]))) (range 1 7))
    (map #(is (= [% (inc %)] (double-if-pair [% (inc %)]))) (range 1 6))
    (is (= [6 1] (double-if-pair [6 1])))
    (is (= [2 5] (double-if-pair [2 5])))))

(deftest is-valid-test
  (testing "Check if move is valid"
    (is (not-any? #(is-valid-move? % -1 player1 board) (range 1 24)))
    (is (not-any? #(is-valid-move? 1 % player1 board) (range -12 1)))
    (is (every? #(is-valid-move? 1 % player0 board) (range 1 7)))
    (is (not-any? #(is-valid-move? 1 % player0 board) (range 7 12)))
    (is (not-any? #(is-valid-move? % 2 player0 board) (range 25 30)))
    (is (not-any? #(is-valid-move? % 2 player0 board) (range -10 1)))
    (is (every? #(is-valid-move? 24 % player1 board) (range -6 0)))
    (is (not-any? #(is-valid-move? % 1 player0 board) (range 2 25)))
    (is (false? (is-valid-move? 1 3 player0 (assoc board 4 [1 1]))))
    (let [game (new-game)]
      (modify-board 1 [1 1 1] game)
      (modify-board 24 [0] game)
      (swap! game assoc :players [(test-player 15 1) (test-player 15 3)])
      (is (is-valid-move? 24 1 (nth (@game :players) 0) (@game :board))))
    (let [game (new-game)]
      (modify-board 2 [1 1 1 1 1] game)
      (modify-board 24 [1 1 1 1 1 1 1 1 1 1] game)
      (is (not (is-valid-move? 2 -4 (test-player 5 3 1) (@game :board)))))))

(deftest player-direction-test
  (testing "Get direction to move the pieces of player"
    (is (= 1 (player-direction player0)))
    (is (= -1 (player-direction player1)))))

(deftest move-test
  (testing "Move"
    (is (every? #(= 14 (count ((move 1 % (new-board)) 1))) (range 2 7)))
    (is (every? #(= 1 (count ((move 1 % (new-board)) %))) (range 2 7)))
    (let [newboard (new-board)]
      (is (= ((move 1 21 (new-board)) 21) [0]))
      (is (= ((move 1 21 (new-board)) 1) [0 0 0 0 0 0 0 0 0 0 0 0 0 0]))
      (is (= ((move 24 21 (move 1 21 (new-board))) 21) [0 1]))
      (is (= ((move 24 21 (move 1 21 (new-board))) 24) [1 1 1 1 1 1 1 1 1 1 1 1 1 1]))
      (move 24 22 newboard)
      (is (= ((move 24 22 (move 24 21 (move 1 21 (new-board)))) 24) [1 1 1 1 1 1 1 1 1 1 1 1 1]))
      (is (= ((move 24 22 (move 24 21 (move 1 21 (new-board)))) 22) [1]))
      (is (= ((move 1 22 (move 24 21 (move 24 21 (move 1 21 (new-board))))) 1) [0 0 0 0 0 0 0 0 0 0 0 0 0]))
      (is (= ((move 1 22 (move 24 22 (move 1 21 (new-board)))) 22) [1 0])))
    (is (every? #(and (= 14 (count ((move 24 % (new-board)) 24)))
                      (= 1 (count ((move 24 % (new-board)) %)))) (range 19 24)))
    (is (let [newboard  (move 1 2 (move 1 3 (move 1 4 (move 1 5 (move 1 6 (move 1 7 (new-board)))))))
              newboard  (move 1 2 (move 1 3 (move 1 4 (move 1 5 (move 1 6 (move 1 7 newboard))))))
              newboard  (move 2 8 (move 3 9 (move 4 10 (move 5 11 (move 6 12 (move 7 13 newboard))))))
              newboard  (move 1 7 (move 7 13 (move 1 7 (move 7 13 newboard))))
              newboard  (move 13 15 (move 13 14 newboard))]
          (and (every? #(= 1 (count (newboard %))) (range 1 16))
               (every? #(= 0 (count (newboard %))) (range 16 23))
               (= 15 (count (newboard 24))))))
    (is (let [newboard (move 13 19 (move 7 13 (move 1 7 (new-board))))]
          (is (complement (is-valid-move? 19 24 player0 newboard)))
          (is (complement (is-valid-move? 19 25 player0 newboard)))))))

(deftest try-move-test
  (testing "Try move. Incorrect data could be passed"
    (let [game (new-game)]
      (try-move 1 1 game)
      (is (= [0] (get (@game :board) 2)))
      (is (= (vec (map (constantly 0) (range 1 15))) (get (@game :board) 1)))
      (try-move 1 6 game)
      (is (= [0] (get (@game :board) 2)))
      (is (= (vec (map (constantly 0) (range 1 14))) (get (@game :board) 1)))
      (is (= [0] (get (@game :board) 7)))
      (try-move 1 2 game)
      (is (= [0] (get (@game :board) 2)))
      (is (= (vec (map (constantly 0) (range 1 13))) (get (@game :board) 1)))
      (is (= [0] (get (@game :board) 7)))
      (is (= [0] (get (@game :board) 3)))
      (try-move 4 2 game)
      (is (= [0] (get (@game :board) 2)))
      (is (= (vec (map (constantly 0) (range 1 13))) (get (@game :board) 1)))
      (is (= [0] (get (@game :board) 7)))
      (is (= [0] (get (@game :board) 3)))
      (try-move 10 2 game)
      (is (= [0] (get (@game :board) 2)))
      (is (= (vec (map (constantly 0) (range 1 13))) (get (@game :board) 1)))
      (is (= [0] (get (@game :board) 7)))
      (is (= [0] (get (@game :board) 3)))
      (try-move -4 2 game)
      (is (= [0] (get (@game :board) 2)))
      (is (= (vec (map (constantly 0) (range 1 13))) (get (@game :board) 1)))
      (is (= [0] (get (@game :board) 7)))
      (is (= [0] (get (@game :board) 3)))
      (try-move 4 -2 game)
      (is (= [0] (get (@game :board) 2)))
      (is (= (vec (map (constantly 0) (range 1 13))) (get (@game :board) 1)))
      (is (= [0] (get (@game :board) 7)))
      (is (= [0] (get (@game :board) 3))))))

(defn winner [number] {:number number :home 0 :out 15})

(deftest win-test2
  (testing "Win"
    (is (complement (win? (new-game))))
    (let [game (new-game)]
      (swap! game assoc :players [(new-player 0) (winner 1)])
      (is (complement (win? game)))
      (swap! game assoc :players [(winner 0) (new-player 1)])
      (is (win? game))
      (swap! game assoc :next 1)
      (is (complement (win? game))))))

(deftest pair-test
  (testing "Pair?"
    (is (every? #(pair? [% %]) (range 1 7)))
    (is (not-any? #(pair? [1 %]) (range 2 7)))
    (is (not-any? #(pair? [% 1]) (range 2 7)))))

(deftest opponent-test
  (testing "Get opponent"
    (let [game (new-game)]
      (is (zero? ((get-next-player game) :number)))
      (is (= 1 ((opponent game) :number)))
      (change-players game)
      (is (= 1 ((get-next-player game) :number)))
      (is (zero? ((opponent game) :number))))))

(deftest get-char-test
  (testing "Get character in ui mode representing black or white player"
    (is (every? #(= (get-char [0 0 0] %) "\u25CF ") (range 0 3)))
    (is (every? #(= (get-char [0 0 0] %) "  ") (range 3 10)))
    (is (every? #(= (get-char [1 1 1] %) "\u25CB ") (range 0 3)))
    (is (every? #(= (get-char [1 0 1 0 1 0 1 0] %) (if (zero? (rem % 2)) "\u25CB " "\u25CF ")) (range 0 8)))))

(deftest change-player-test
  (testing "Changing of players"
    (let [game (new-game)]
      (change-players game)
      (is (= 0 (:next (change-players game))))
      (change-players game)
      (is (=  (:next (change-players game))))
      (change-players game)
      (is (= 0 (:next (change-players game))))
      (change-players game)
      (is (=  (:next (change-players game)))))))

(deftest update-test
  (testing "Update player status"
    (let [next-player (new-player 0) other-player (new-player 1)]
      ;"Move in the beginning of the board without change in player's info"
     (is (= {:home 0 :out 0 :number 0} (update 1 2 next-player)))
     (is (= {:home 0 :out 0 :number 0} (update 21 -1 next-player)))
      ;"Move in home"
     (is (= {:home 1 :out 0 :number 0} (update 18 1 next-player)))
     (is (= {:home 1 :out 0 :number 0} (update 13 6 next-player)))
     (is (= {:home 1 :out 0 :number 0} (update 18 6 next-player)))
     (is (= {:home 1 :out 0 :number 0} (update 7 -1 next-player)))
     (is (= {:home 1 :out 0 :number 0} (update 7 -6 next-player)))
     (is (= {:home 1 :out 0 :number 0} (update 11 -5 next-player))))
    (let [next-player (test-player 15 0)]
      ;"Move in home should not change in player's info"
      (is (= {:number 0 :out 0 :home 15} (update 22 1 next-player)))
      (is (= {:number 0 :out 0 :home 15} (update 22 2 next-player)))
      (is (= {:number 0 :out 0 :home 15} (update 19 1 next-player)))
      (is (= {:number 0 :out 0 :home 15} (update 19 5 next-player)))
      (is (= {:number 0 :out 0 :home 15} (update 22 2 next-player)))
      (is (= {:number 0 :out 0 :home 15} (update 3 -2 next-player)))
      (is (= {:number 0 :out 0 :home 15} (update 6 -5 next-player)))
      (is (= {:number 0 :out 0 :home 15} (update 6 -1 next-player)))
      (is (= {:number 0 :out 0 :home 15} (update 2 -1 next-player)))
      (is (= {:number 0 :out 0 :home 15} (update 4 -1 next-player)))
       ;"Move out"
      (is (= {:number 0 :out 1 :home 15} (update 22 3 next-player)))
      (is (= {:number 0 :out 1 :home 15} (update 21 6 next-player)))
      (is (= {:number 0 :out 1 :home 15} (update 19 6 next-player)))
      (is (= {:number 0 :out 1 :home 15} (update 24 5 next-player)))
      (is (= {:number 0 :out 1 :home 15} (update 23 2 next-player)))
      (is (= {:number 0 :out 1 :home 15} (update 3 -3 next-player)))
      (is (= {:number 0 :out 1 :home 15} (update 6 -6 next-player)))
      (is (= {:number 0 :out 1 :home 15} (update 4 -5 next-player)))
      (is (= {:number 0 :out 1 :home 15} (update 1 -1 next-player)))
      (is (= {:number 0 :out 1 :home 15} (update 1 -6 next-player))))))

(deftest all-possible-moves-test
  (testing "Calculate all possible moves for given steps"
    (let [game (new-game)]
      (map #(is (= [1] (all-possible-moves game %))) (range 1 7))
      (change-players game)
      (map #(is (= [24] (all-possible-moves game %))) (range 1 7))
      (modify-board 21 [0 0] game)
      (change-players game)
      (map #(is (= [1 21] (all-possible-moves game %))) (range 1 3))
      (map #(is (= [1] (all-possible-moves game %))) (range 3 7))
      (change-players game)
      (map #(is (= [24] (all-possible-moves game %))) (range 1 3))
      (is (= [] (all-possible-moves game 3)))
      (map #(is (= [24] (all-possible-moves game %))) (range 4 7)))
     (let [game (new-game)]
      (modify-board 1 [1 1 1] game)
      (modify-board 24 [0] game)
      (map #(is (= [24] (all-possible-moves game %))) (range 1 7)))))

(deftest computer-turn-test
  (testing "Computer final turn"
    (let [game (new-game)]
      (modify-board 1 [1 1 1] game)
      (modify-board 24 [0] game)
      (swap! game assoc :players [(test-player 15 14) (test-player 15 12)])
      (roll-dice game)
      (computer-turn game)
      (is (win? game)))))

(deftest computer-move-twice
  (testing "Twice computer move"
     (let [game (new-game)]
       (roll-dice game)
       (computer-turn game)
       (change-players game)
       (roll-dice game)
       (computer-turn game)
       (change-players game)
       (is (false? (win? game))))))

(deftest win-test
  (testing "Win after last move"
    (let [game (new-game)]
      (modify-board 1 [1 1 1] game)
      (modify-board 24 [0] game)
      (swap! game assoc :players [(test-player 15 14) (test-player 15 12)])
      (is (is-valid-move? 24 1 (nth (@game :players) 0) (@game :board)))
      (roll-dice game)
      (computer-turn game)
      (is (win? game))
      (is (empty? (@game :dices)))
      (is (empty? (all-possible-moves game 1))))
    (let [game (new-game)]
      (modify-board 1 [1 1 1] game)
      (modify-board 24 [0 0] game)
      (swap! game assoc :players [(test-player 15 13) (test-player 15 12)])
      (is (is-valid-move? 24 1 (nth (@game :players) 0) (@game :board)))
      (swap! game assoc :dices [1 2])
      (computer-turn game)
      (is (win? game))
      (is (empty? (@game :dices)))
      (is (empty? (all-possible-moves game 1))))
    (let [game (new-game)]
      (modify-board 1 [1 1 1] game)
      (modify-board 24 [0 0 0 0] game)
      (swap! game assoc :players [(test-player 15 11) (test-player 15 12)])
      (is (is-valid-move? 24 1 (nth (@game :players) 0) (@game :board)))
      (swap! game assoc :dices [1 1 1 1])
      (computer-turn game)
      (is (win? game))
      (is (empty? (@game :dices)))
      (is (empty? (all-possible-moves game 1))))
    (let [game (new-game)]
      (modify-board 1 [1  1] game)
      (modify-board 24 [0 0 0] game)
      (swap! game assoc :players [(test-player 15 12) (test-player 15 12)] :dices [1 1 1 1])
      (computer-turn game)
      (is (win? game))
      (is (complement (empty? (@game :dices))))
      (is (empty? (all-possible-moves game 1))))))


(deftest ai-test
  (testing "Computer vc Computer"
    (let [game (new-game)]
      (loop []
        (roll-dice game)
        (computer-turn game)
        (if (not (win? game))
          (do
            (change-players game)
            (recur)))))))

(deftest no-more-moves?-test
  (testing "Test if moves are available"
    (let [game (new-game)]
      (modify-board 3 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1] game)
      (modify-board 1 [0 0 0 0 0 0 0 0 0 0 0 0 0] game)
      (modify-board 24 [] game)
      (modify-board 2 [0 0] game)
      (change-players game)
      (swap! game assoc :dices [2 2 2 2] :players [(test-player 0 0) (test-player 15 0 1)] :next 1)
      (is (no-more-moves? game))
      (modify-board 4 [1 1 1 1 1 1 1 1 1 1 1 1 1] game)
      (modify-board 3 [1 1] game)
      (swap! game assoc :dices [2 2])
      (is (no-more-moves? game))
      (swap! game assoc :dices [2])
      (is (no-more-moves? game))
      (swap! game assoc :dices [2 2 2 2])
      (is (no-more-moves? game))
      (swap! game assoc :dices [2 2 2])
      (is (no-more-moves? game))
      (swap! game assoc :dices [1 2])
      (is (false? (no-more-moves? game)))
      (swap! game assoc :dices [1])
      (is (false? (no-more-moves? game)))
      (swap! game assoc :dices [6])
      (is (false? (no-more-moves? game)))
      (swap! game assoc :dices [2 6])
      (is (false? (no-more-moves? game)))
      (swap! game assoc :dices [1 1 1 1])
      (is (false? (no-more-moves? game)))
      (swap! game assoc :dices [1])
      (is (false? (no-more-moves? game)))
      (swap! game assoc :dices [2 6])
      (is (false? (no-more-moves? game))))))
