# tapa

Tapa is a popular version of backgammon played in Bulgaria, where the word tapa means "bottle cap." The goal in this game is to trap your opponent's checkers rather than hit them.

## Installation

Download from https://bitbucket.org/elenadeneva1/tapa

## Usage
To start a game run

    $ java -jar tapa-0.1.0-standalone.jar [args]

## License

Copyright © 2014 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
